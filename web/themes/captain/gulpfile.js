var gulp = require('gulp'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  environments = require('gulp-environments');

var development = environments.development;
var production = environments.production;

var sass_path = [
  './scss/**/*.+(scss|sass)'
];

gulp.task('default', function () {
  gulp.start('scss');
});

gulp.task('scss', function () {
  environments.current(development);

  console.log('Compiling scss on ' + environments.current().$name + ' env.');
  gulp.src(sass_path)
        .pipe(development(sourcemaps.init()))
        .pipe(sass(
          {
            includePaths: [
              './node_modules/font-awesome/scss'],
            style: 'expanded'
          }
        ).on('error', sass.logError))
        .pipe(development(sourcemaps.write()))
        .pipe(gulp.dest('./css'));
});

gulp.task('watch', ['scss'], function () {
  gulp.watch(sass_path, ['scss']);
});

gulp.task('set-prod', production.task);
gulp.task('build', ['set-prod', 'libs', 'scss']);

gulp.task('libs', function () {
  var Lib = function (name, src, dest) {
    this.name = name;
    this.src = src;
    this.dest = dest;
  };
  var libs = [];
  libs.push(new Lib('fa_fonts', 'node_modules/font-awesome/fonts/**/*', './fonts/FontAwesome/'));
  libs.forEach(function (lib, n, libs) {
    console.log('Handling ' + lib.name + ' library.');
    var res = gulp.src(lib.src)
            .pipe(gulp.dest(lib.dest));
        // console.log(res);
  });
});
