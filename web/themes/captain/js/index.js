(function($, Drupal) {
  'use strict';

  Drupal.behaviors.readMore = {
    attach: function(context, settings) {
      var scrollIcon = $('.scroll-icon_block', context);

      $(scrollIcon).click(function() {
        var vh = $('.field--name-field-background', context).height();
        $('html, body').animate({ scrollTop: vh }, 300, 'swing')
      })
    }
  }


  Drupal.behaviors.fixed_mmenu = {
    attach: function(context, settings) {
      var chooseBtn = $('.field--name-field-choose-button', context);
      var burgerBtn = $(context).find('body #hamburger');
      var fixedMenu = $(context).find('body .fixed-mmenu-content');
      var menuOverlay = $(context).find('body .fixed-mmenu-overlay');
      var menuItem = fixedMenu.find('ul > li > a');

      burgerBtn.click(function() {
        fixedMenu.toggleClass('opened');
      });

      $("a[href^='#']", context).click(function(e) {
        smoothScroll(e, this)
      })

      menuOverlay.click(function() {
        fixedMenu.toggleClass('opened');
      });

      menuItem.click(function(e) {
        var opened = $(this).parent().hasClass('open');

        smoothScroll(e, this)
        if (!($(this).parent().hasClass('dropdown')) || opened) {
          fixedMenu.toggleClass('opened');
        }
      });

      var firstLevel = $(context).find('body').find('.dropdown-toggle');

      firstLevel.click(function(e) {
        if ($(this).parent().hasClass('open')) {
          smoothScroll(e, this);
        }
      });


      var smoothScroll = (e, link) => {
        if ($.attr(link, 'href').includes('#')) {
          if ($.attr(link, 'href').length != 1) {
            if ($.attr(link, 'href').includes('/')) {
              var newLink = $.attr(link, 'href').substr('1')
            } else {
              var newLink = $.attr(link, 'href')
            }
            e.preventDefault()

            $('html, body').animate({
              scrollTop: $(newLink).offset().top
            }, 300)
          }
        }
      }
    }
  }

  Drupal.behaviors.animatedHamburger = {
    attach: function(context, settings) {
      var trigger = $('#hamburger', context);
      var menuOverlay = $('body .fixed-mmenu-overlay', context);
      var fixedMenu = $('body .fixed-mmenu-content', context);
      var menuItem = fixedMenu.find('ul > li > a');

      menuItem.click(function() {
        var opened = $(this).parent().hasClass('open');
        if (!($(this).parent().hasClass('dropdown')) || opened) {
          burgerTime();
        }
      });

      menuOverlay.click(function() {
        burgerTime();
      });

      trigger.click(function() {
        burgerTime();
      });


      function burgerTime() {
        if (!(trigger.hasClass('is-closed')) && !(trigger.hasClass('is-open'))) {
          trigger.addClass('is-open');
        } else if (trigger.hasClass('is-open')) {
          trigger.removeClass('is-open');
          trigger.addClass('is-closed');
        } else {
          trigger.removeClass('is-closed');
          trigger.addClass('is-open');
        }


      }
    }
  };
})(jQuery, Drupal);
