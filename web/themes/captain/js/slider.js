(function ($, Drupal) {
  'use strict';


  Drupal.behaviors.routesSlider = {
    attach: function (context, settings) {
      var slider = $('.routes .routes-slider', context);

      slider.slick(
        {
          infinite: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          centerMode: true,
          centerPadding: '0',
          variableWidth: true,
          // adaptiveHeight: true,
          dots: true,
          autoplay: true,
          autoplaySpeed: 2500,
          responsive: [
            {
              breakpoint: 992,
              settings: {
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
              }
            },
            {
              breakpoint: 769,
              settings: {
                variableWidth: false,
                centerMode: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                infinite: true
              }
            }
          ]
        }
      );

      var form = $('.routes .routes-form', context);
      var form_close = $('.close-btn > i', form);
      var overlay = $('.routes_overlay', context);
      var body = $('body', context);
      function close_form() {
        form.removeClass('opened');
        overlay.removeClass('opened');
        body.removeClass('no-scroll');
      }
      slider.on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        close_form();
      });
      form_close.click(close_form);
      overlay.click(close_form);

      function setSelect(val) {
        var select = $('[name="route"]', form);
        select.val(val);
      }
      $('.routes-slide', slider).each(function () {
        var route = $(this).data('route');
        var btn = $('.download', this);
        if (form.hasClass('first_submit')) {
          btn.on('click', { route:route }, function(e) {
            e.preventDefault();
            setSelect(e.data.route);
            form.addClass('opened');
            overlay.addClass('opened');
            body.addClass('no-scroll');
          })
        } else {
          btn.on('click', function (e) {
            e.preventDefault();
            setSelect(route);
            $('button[name="op"]', form).trigger('click');
          })
        }
      });

    }
  };
})(jQuery, Drupal);
