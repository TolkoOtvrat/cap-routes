(function($, Drupal) {
  'use strict';

  Drupal.behaviors.routes_browser = {
    attach: function(context, settings) {
      var routesWrapper = $('.routes_wrapper', context);

      // FILTER OBJ
      var routeItem = $('.route-item', routesWrapper);

      $(routesWrapper).each(
        function() {
          var routeObj = new RouteCheckList(this);
          // MODAL WINDOW 

          var modal = $('.download-form-wrapper', routesWrapper);
          var overlay = $('.overlay', modal);
          var downloadBtn = $('.download.btn', routesWrapper);
          var closeBtn = $('.close-btn', modal);
          var deleteFromAnchor = $('.deleteBtn', '.download-anchor-modal');

          $(downloadBtn).addClass('inactive-btn');

          $(downloadBtn).click(function(e) {
            e.preventDefault();
            if (!($(this).hasClass('inactive-btn'))) {
              $(modal).addClass('opened');
            }
          })

          $(closeBtn).click(function() {
            $(modal).removeClass('opened');
          })

          $(overlay).click(function() {
            $(modal).removeClass('opened');
          })

          // Delete from anchor 

          $(document).on('click', '.deleteBtn', function() {
            var _id = $(this).data('route-id');
            routeObj.findRouteById(_id);
          })
          // WARNING MODAL 

          var warningWindow = $('.warning', context);
          var closeBtn = $('.close-btn', warningWindow);
          var okBtn = $('.btn', warningWindow);

          $(closeBtn).click(function(e) {
            warningWindow.removeClass('opened');
          })

          $(okBtn).click(function(e) {
            e.preventDefault();
            warningWindow.removeClass('opened');
          })


          // FILTER

          routeObj.setCountryList(settings.routes_country);

          $('.select').select2({
            placeholder: "Выберите страну",
            allowClear: true,
            data: routeObj.getCountryList()
          })

          $('.select').on('select2:select', function(evt) {
            routeObj.setFilterId(evt.params.data.id);
          })

          $('.select').on('select2:unselect', function(evt) {
            var clearId = '';
            routeObj.setFilterId(clearId);
          })

          $(routeItem).click(function() {
            routeObj.setCheckRoute(this);
          })
        })
    }
  }

  function RouteCheckList(wrapper) {
    var self = this;
    var routesWrapper = wrapper;
    var MAX_CHECKED_ITEMS = 3;
    var ITEM_TO_SHOW = 12;

    var select = $('select', 'form.webform-submission-form');
    var downloadBtn = $('.download.btn', routesWrapper);
    var showMore = $('#show_more', routesWrapper);
    var routeItem = $('.route-item', routesWrapper);
    var anchorModal = $('.download-anchor-modal', routesWrapper);
    var checkedItems = [];
    var countryList = [{
      id: '',
      text: ''
    }];

    function InitialItemCount() {
      if (!($(routesWrapper).find(routeItem).length <= ITEM_TO_SHOW)) {
        $(routeItem).each(function(index) {
          $(showMore).removeClass('hidden');
          if (index >= ITEM_TO_SHOW) {
            $(this).addClass('hidden');
          }
        })
      } else {
        $(showMore).addClass('hidden');
      }
    }

    InitialItemCount();

    $(showMore).click(function(e) {
      e.preventDefault();
      $(this).addClass('hidden');
      $('.route-item', routesWrapper).removeClass('hidden');
    })

    $('.anchor-btn', anchorModal).click(function() {
      $(anchorModal).toggleClass('open');
    })

    // METHODS FOR FILTER

    this.setFilterId = function(id) {
      startFilter(id);
    }

    function startFilter(id) {
      if (id != '') {
        var elements = $(".route-item[data-countries*='" + id + "']");
        $('.route-item').addClass('hidden');
        $(elements).removeClass('hidden');
      } else {
        $(routeItem).addClass('hidden');
        $(routeItem).removeClass('hidden');
        InitialItemCount();
      }
    }

    // GENERATE COUNRIES ARROW

    this.setCountryList = function(list) {
      regenerateArray(list);
    }

    this.getCountryList = function() {
      return countryList;
    }

    function regenerateArray(list) {
      var key = Object.keys(list);

      for (key in list) {
        var currArray = new Object();

        currArray.id = key;
        currArray.text = list[key];
        countryList.push(currArray);
      }
    }

    // METHODS FOR CHECK

    this.findRouteById = function(routeId) {
      var currRoute = $(routesWrapper).find("[data-route-id='" + routeId + "']")[0]; 
      UncheckRoute(currRoute);
    }

    this.setCheckRoute = function(route) {
      if (checkedItems.indexOf(route) == -1 && checkedItems.length < MAX_CHECKED_ITEMS) {
        CheckRoute(route);
      } else if (checkedItems.indexOf(route) != -1) {
        UncheckRoute(route);
      } else {
        warningWindow();
      }
    }

    function warningWindow() {
      $('.warning', routesWrapper).addClass('opened');
    }

    function CheckRoute(route) {
      var select = $('form.webform-submission-form').find('select');
      checkedItems.push(route);
      $(select).val(checkedItems);
      $('.field--name-field-route-image', route).addClass('checked');
      Selection();
    }

    function UncheckRoute(route) {
      var itemIndex = checkedItems.indexOf(route);
      checkedItems.splice(itemIndex, 1)
      $('.field--name-field-route-image', route).removeClass('checked');
      Selection();
    }

    function Selection() {
      var ids = []
      for (var i = 0, len = checkedItems.length; i < len; i++) {
        var id = $(checkedItems[i]).attr('data-route-id');
        ids.push(id);
      }
      $(select).val(ids);
      activateModal();
      activateAnchorModal();
      pushToAnchor();
    }

    function pushToAnchor() {
      var routesToShow = [];
      var divForRoutes = $('.routes-to-download > .routes', '.download-anchor-modal')
      divForRoutes[0].innerHTML = '';

      for (var i = 0, len = checkedItems.length; i < len; i++) {
        var routeImg = $('.field--name-field-route-image > img', checkedItems[i]).clone();
        var routeTitle = $('.route-title', checkedItems[i]).text();
        var routeCountry = countryChecker($(checkedItems[i]).attr('data-countries'));
        var routeId = $(checkedItems[i]).attr('data-route-id');

        //Route obj to items to show
        var currArray = new Object();
        currArray.img = routeImg;
        currArray.title = routeTitle;
        currArray.country = routeCountry;
        currArray.id = routeId;

        routesToShow.push(currArray);
      }

      // Create DOM
      for (var i = 0, len = routesToShow.length; i < len; i++) {
        // div for each route
        var routeToShowDiv = document.createElement("div");
        $(routeToShowDiv).addClass('route-to-show');

        // create elements for title & country
        var textDiv = document.createElement("div");
        $(textDiv).addClass('right-text');
        var routeToShowTitle = document.createElement("p");
        routeToShowTitle.innerHTML = routesToShow[i].title;
        var routeToShowCountry = document.createElement("span");
        routeToShowCountry.innerHTML = routesToShow[i].country;
        var deleteBtn = document.createElement("span");
        deleteBtn.innerHTML = "Удалить из списка";
        $(deleteBtn).addClass('deleteBtn').attr('data-route-id', routesToShow[i].id);

        $(textDiv).append(routeToShowTitle, routeToShowCountry, deleteBtn);

        // append to div.route-to-show
        $(routeToShowDiv).append(routesToShow[i].img, textDiv);

        // append div.route-to-show to HTML
        divForRoutes.append(routeToShowDiv);
      }
    }

    function countryChecker(routeId) {
      var _routeId = routeId.split(" ");
      var countries = [];

      for(var i = 0, len = _routeId.length; i < len; i++) {
        var result = $.grep(countryList, function(e) {
          return e.id == _routeId[i];
        });
        countries.push(result[0].text);
      }

      return countries.join(", ");
    }

    function activateAnchorModal() {   
      if (checkedItems.length != 0) {
        $(anchorModal).addClass('show');
        if (screen.width > 592) {
          $(anchorModal).addClass('open');
        }
      } else {
        $(anchorModal).removeClass('show');
        $(anchorModal).removeClass('open');
      }
    }
 
    function activateModal() {
      if (checkedItems.length != 0) {
        $(downloadBtn).removeClass('inactive-btn');
      } else {
        $(downloadBtn).addClass('inactive-btn');
      }
    }
  }

})(jQuery, Drupal);
