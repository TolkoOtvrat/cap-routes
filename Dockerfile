FROM php:7-apache

RUN apt-get update && apt-get install -y \
  vim \
  php5-mysql \
  git \
  unzip \
  mysql-client

#Php deps
RUN apt-get install -y \
  zlib1g-dev \
  libpng-dev \
  libjpeg62-turbo-dev \ 
  libfreetype6-dev \
  libjpeg62-turbo 

RUN docker-php-ext-install \
  mysqli \
  pdo \
  pdo_mysql \
  zip \ 
  opcache

RUN docker-php-ext-configure \
  gd --with-freetype-dir=/usr/include/ \
     --with-jpeg-dir=/usr/include/ \
     && docker-php-ext-install gd

RUN pecl install xdebug && \
  docker-php-ext-enable xdebug

COPY config/php-dev.ini /usr/local/etc/php/conf.d/
COPY config/apache_vhost.conf /etc/apache2/sites-available/
RUN a2dissite 000-default && \
  a2ensite apache_vhost
RUN usermod -u 1000 www-data && \
  a2enmod rewrite headers expires
