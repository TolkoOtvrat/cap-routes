#!/bin/bash - 

#set -o nounset                              # Treat unset variables as an error

proot=$(git rev-parse --show-toplevel)

cd $proot

docker-compose run --rm web /var/www/composer.phar install
docker-compose run --rm web drush updb 
docker-compose run --rm web drush cim 
docker-compose run --rm web drush cr
