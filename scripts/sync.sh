#!/bin/bash -
#===============================================================================
#
#          FILE: sync_from.sh
#
#         USAGE: ./sync_from.sh
#
#   DESCRIPTION:
#
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: YOUR NAME (),
#  ORGANIZATION:
#       CREATED: 19.06.2017 18:18
#      REVISION:  ---
#===============================================================================

#set -o nounset                              # Treat unset variables as an error

proot=$(git rev-parse --show-toplevel)

cd "$proot/web"
if [ -z "$2" ]; then
  to="@self"
else
  to="$2"
fi

if [ -z "$1" ]; then
  echo "Usage: ./sync.sh @snpdev.stage [ @self ]."
  echo "Second argument is optional and set to \"@self\" by default."
  exit 1;
else
  from="$1";
fi

../drush.wrapper sql-sync $from $to -y
../drush.wrapper rsync $from:%files $to:%files -y
../drush.wrapper rsync $from:%private $to:%private -y
